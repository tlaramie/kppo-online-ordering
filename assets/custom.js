$(document).ready(function () { 
  $('h4.clicker').click(function(){
    if (!$(this).hasClass('active')){
      $('div.product-content').slideUp();
      $(this).next().slideToggle();
      $(".x-icon").removeClass('rotate-x');
      $(".x-icon", this).addClass('rotate-x');
      $('h4.clicker').removeClass('active');
      $(this).addClass('active');
      return false;
    } else {
      $('div.product-content').slideUp();
      $(".x-icon").removeClass('rotate-x');
      $(".x-icon").not(".x-icon", this).removeClass('rotate-x');
      $('h4.clicker').removeClass('active');
      return false;
    }
  });
});